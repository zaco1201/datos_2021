#include <stdio.h>
int main()
{
	/**Se devuelve el factorial del numero ingresado por el usuario*/
	int n = 0;
	int i = 0;
    int factorial = 1;
    printf("Ingrese un numero entero: ");
    scanf("%d", &n);

    // Muestra mensaje de error en caso de que no sea un numero positivo el ingresado
    if (n < 0)
        printf("No ingreso un numero positivo!");
    else {
		//
        for (i = 1; i <= n; ++i) {
            factorial *= i;
        }
        printf("El factorial de %d es %d \n", n, factorial);
    }
}
