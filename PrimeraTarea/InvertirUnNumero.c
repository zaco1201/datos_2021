#include <stdio.h>
int main()
{
	/** Invierte el numero ingresado por el usuario */
	int n = 0;
	int resultado = 0;
	
	printf("Ingrese un numero entero: ");
	scanf("%d", &n);
	
	while(n != 0){
		resultado = resultado*10;
		resultado = resultado+n%10;
		n = n/10;
	}
	printf("%d \n", resultado);
}
