#include <stdio.h>
/** Este programa se encarga de recibir un numero entero escrito por el
 usuario y devolver un mensaje diciendo si este es par o impar*/
int main()
{
	int valorEntrada = 0;
	printf("Ingrese un numero entero: ");
	//Se guarda el numero ingresado por el usuario en la variable valorEntrada
	scanf("%d", &valorEntrada);

	//Se utiliza modul(%2) en caso de que el resulato sea 0 el numero ingresado seria par y en caso de ser 1 seria impar
	if (valorEntrada % 2 == 0){
	  printf("El numero es par \n");
	} else {
	  printf("El numero es impar \n");
	}
}
