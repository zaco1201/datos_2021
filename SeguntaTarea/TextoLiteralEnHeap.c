#include <stdio.h>
#include <string.h>
#include <stdlib.h>

int main()
{
	/** Recibe un texto del usuario y mediante a funcion calloc lo reescribe
	 en el heap*/
	char textoLiteral[100];
	printf("Ingrese el texto que desee: ");
	scanf("%[^\n]", textoLiteral);
	char* largoTexto = strlen(textoLiteral);
	int contador = 0;
	
	/*Lo manda al heap mediante el calloc*/
	char* heap = calloc(largoTexto + 1, sizeof(char));
	
	/* Se encarga de reescribir el texto*/
	while(textoLiteral[contador] != '\0'){
		heap[contador] = textoLiteral[contador];
		contador++;
	}
	
	printf("El texto en el heap es: %s \n", heap);
	
	return 0;
}
