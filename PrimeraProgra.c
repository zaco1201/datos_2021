#include <stdio.h>
#include <stdlib.h>
#include <time.h>

struct nodo {
    char *valor;
    int *vida;
    int *damage;
    struct nodo *sigt;
    struct nodo *prev;
	};

struct lista_enlazada{
	struct nodo *inicio;
	};

struct lista_enlazada* crear_lista(){
	struct lista_enlazada *nueva_lista = calloc(1, sizeof(struct lista_enlazada));
	return nueva_lista;
	}

struct nodo* crear_nodo(){

	struct nodo* nuevo_nodo = calloc(1, sizeof(struct nodo));
	return nuevo_nodo;
	}

int imprimir_lista(struct lista_enlazada* lista){

	// Seccion para definir variables
	struct nodo* actual = NULL;

	// Lista vacia
	if (lista->inicio == NULL){
		printf("Lista vacía\n");
		return -2;
		}

	// pongo un apuntador adicional al inicio.
	actual = lista->inicio;
	printf("| ");
	while (actual != NULL){
		printf("%s | \n", actual->valor);
		printf("  %d  \n", actual->vida);
		printf("  %d  \n", actual->damage);
		actual = actual->sigt;
		}

	printf("\n");
	return 0;
}

struct lista_enlazada* insertar(struct lista_enlazada* lista, char* valor, int *vida, int *damage){

	// Definicion de variables
	struct nodo* nuevo_nodo = NULL;
	struct nodo* actual = NULL;

	// Crear el nodo y asignarle el valor
	nuevo_nodo = crear_nodo();
	nuevo_nodo->valor = valor;
	nuevo_nodo->vida = vida;
	nuevo_nodo->damage = damage;
	
	// Si la lista esta vacia
	if (lista->inicio == NULL){
		lista->inicio = nuevo_nodo;
		}
	
	// La lista no esta vacia
	else{
		actual = lista->inicio;

		while(actual->sigt!=NULL){
			actual = nuevo_nodo;
			actual = actual->sigt;
			}
		}
		
	return lista;
}

int main(void) {

	struct nodo* resultado = NULL;
	struct lista_enlazada *lista = crear_lista();
	struct lista_enlazada *lista2 = crear_lista();
	printf("------------------\n");
	printf("Inicio del juego\n");

	insertar(lista, "x", 5,6);
	insertar(lista, "O", 5,6);
	imprimir_lista(lista);
	
	return 0;
	}
