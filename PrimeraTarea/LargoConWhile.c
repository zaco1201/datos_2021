#include <stdio.h>
int main()
{
	/** Se devuelve la cantidad de digitos del numero ingresado por el usuario
	utilizando el ciclo while*/
	int i = 0;
	int valorEntrada = 0;
	printf("Digite un numero entero: ");
	scanf("%d", &valorEntrada);
	int aux = valorEntrada;

	/* Se utiliza el i como elemento de saida del while y como contador
	de la cantidad de digitos del numero y se ira recorriendo utilizando
	la division completa entra 10*/
	while(valorEntrada != 0){
		i++;
		valorEntrada = valorEntrada/10;
	}
	printf("%d \n", i);
}
