#include <stdio.h>
int main()
{
	/** Revisa si el numero ingresado por el usuario es un numero palindromo
	y devuelve un mensaje de si este es o no */
	int primero = 0;
	int n = 0;
	int r = 0;
	
	printf("Ingrese un numero entero: ");
	scanf("%d", &n);
	primero = n;
	
	while(n != 0){
		r = r*10;
		r = r+n%10;
		n = n/10;
	}
	if (primero == r){
		printf("El numero %d es palindromo \n", primero);
	}
	else{
		printf("El numero %d no es palindromo \n", primero);
	}
}
