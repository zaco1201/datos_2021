#include <stdio.h>
int main()
{	
	/** Se devuelve la cantidad de digitos del numero ingresado por el usuario
	utilizando el ciclo for*/
	int i;
	int valorEntrada = 0;
	printf("Digite un numero entero: ");
	scanf("%d", &valorEntrada);
	int aux = valorEntrada;
	
	/*Se utiliza utiliza el i para contar la cantidad de digitos del numero ingresado
	mientras que se utiliza la division completa entre 10 para ir recorriendo el numero de izquierda
	a derecha*/
	for(i = 0; aux >0; i++){
		
		aux = aux/10;
	}
	printf("%d \n", i);
}


